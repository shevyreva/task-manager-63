package ru.t1.shevyreva.tm.jms.operation;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
