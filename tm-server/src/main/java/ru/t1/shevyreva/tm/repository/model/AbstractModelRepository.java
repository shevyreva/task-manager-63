package ru.t1.shevyreva.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.model.AbstractModel;


@Repository
public interface AbstractModelRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}
