package ru.t1.shevyreva.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull final String json);

}
