package ru.t1.shevyreva.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.api.service.model.ISessionService;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.model.Session;
import ru.t1.shevyreva.tm.repository.model.SessionRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository sessionRepository;

    @NotNull
    @SneakyThrows
    @Transactional
    public Session add(
            @Nullable final String userId,
            @Nullable final Session session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        //session.setUserId(userId);
        add(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Session add(
            @Nullable final Session session) {
        sessionRepository.saveAndFlush(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final Session session = sessionRepository.findByUserIdAndId(userId, id);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Session removeOne(@Nullable final Session session) {
        if (session == null) throw new ModelNotFoundException();

        sessionRepository.delete(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Session removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        Session session = findOneById(userId, id);
        sessionRepository.deleteById(id);
        return session;
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        sessionRepository.deleteByUserId(userId);
    }

    @SneakyThrows
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<Session> add(@NotNull final Collection<Session> models) {
        if (models == null) throw new ModelNotFoundException();

        for (@NotNull Session session : models) {
            add(session);
        }
        return models;
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        sessionRepository.deleteAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<Session> set(@NotNull final Collection<Session> models) {
        @Nullable final Collection<Session> entities;
        removeAll();
        entities = add(models);
        return entities;
    }

    public boolean existsById(@NotNull final String id) {
        return (sessionRepository.findById(id) != null);
    }

    @NotNull
    public long getSize() {
        @Nullable final long count = sessionRepository.count();
        return count;
    }

}
