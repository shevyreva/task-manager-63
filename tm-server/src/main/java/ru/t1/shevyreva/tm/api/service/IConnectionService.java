package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    public EntityManager getEntityManager();

    @NotNull
    public EntityManagerFactory factory();

}
