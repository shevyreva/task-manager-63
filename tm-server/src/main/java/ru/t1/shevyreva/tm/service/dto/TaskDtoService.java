package ru.t1.shevyreva.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.api.service.dto.ITaskDtoService;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskDtoSort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.repository.dto.TaskDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        add(userId, task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        taskRepository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);

        taskRepository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final TaskDtoSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        if (sort == null) return taskRepository.findByUserId(userId);
        if (sort != null) return taskRepository.findAllByUserIdWithSort(userId, getSortType(sort));
        else return null;
    }

    @NotNull
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final TaskDTO task = taskRepository.findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        return taskRepository.countByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO removeOne(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new ModelNotFoundException();

        taskRepository.deleteOneByIdAndUserId(userId, task.getId());
        return task;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        TaskDTO task = taskRepository.findOneByUserIdAndId(userId, id);
        taskRepository.findOneByUserIdAndId(userId, id);
        return task;
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        taskRepository.deleteByUserId(userId);
    }

    @SneakyThrows
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<TaskDTO> add(@NotNull final Collection<TaskDTO> models) {
        if (models == null) throw new TaskNotFoundException();

        for (@NotNull TaskDTO task : models) {
            add(task);
        }
        return models;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO add(@NotNull final TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();

        taskRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO add(@NotNull final String userId, @NotNull final TaskDTO model) {
        if (model == null) throw new ProjectNotFoundException();

        model.setUserId(userId);
        taskRepository.saveAndFlush(model);
        return model;
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        @Nullable final Collection<TaskDTO> entities;
        removeAll();
        entities = add(models);
        return entities;
    }

    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return (taskRepository.findOneByUserIdAndId(userId, id) != null);
    }

    @NotNull
    private String getSortType(@NotNull TaskDtoSort sort) {
        if (sort == TaskDtoSort.BY_CREATED) return "name";
        else if (sort == TaskDtoSort.BY_STATUS) return "status";
        else return "created";
    }

}
