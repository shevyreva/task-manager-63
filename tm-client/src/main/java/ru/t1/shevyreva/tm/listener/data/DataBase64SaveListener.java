package ru.t1.shevyreva.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.data.DataSaveBase64Request;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save Data to base64 file.";

    @NotNull
    private final String NAME = "data-save-base64";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64SaveListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final DataSaveBase64Request request = new DataSaveBase64Request(getToken());
        domainEndpoint.saveDataBase64(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
