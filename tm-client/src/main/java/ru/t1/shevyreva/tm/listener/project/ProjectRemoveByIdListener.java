package ru.t1.shevyreva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Remove project by Id.";

    @NotNull
    private final String NAME = "project-remove-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        projectEndpoint.removeProjectById(request);
    }

}
